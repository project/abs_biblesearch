<div id="abs-biblesearch-results">
  <div id="abs-bible-search-meta">
    <div id="metainfo">
      <p id="metafor">You searched for: <?php echo $_REQUEST['keys']; ?></p>
      <p id="metacharacteristics">YOUR SEARCH: <strong><?php echo $_REQUEST['keys']; ?></strong> Versions: <strong><?php foreach( $_REQUEST['versions'] as $version ) {
      echo "$version ";
      } ?></strong></p>
    </div>
    
    <div class="clear"></div>
  </div>

  <div id="abs-search-results">
  <?php
  foreach ($passages as $passage) {
    echo theme( 'abs_biblesearch_verse_passage', array(
      'passage' => $passage, 
      'version' => $version
      ) );
  }
  ?>
  </div>
</div>